﻿using Microsoft.Extensions.Configuration;
using RetaguardaWs.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Database
{
    public class SQLServer
    {
        private bool disposed = false;

        // atributos
        private SqlConnection connection;
        private SqlTransaction transaction;
        private string connectionString;

        //----------------------------//-----------------------------//
        public SQLServer(string conn)
        {
            this.connectionString = "";

            try
            {
                this.connectionString = Program.Configuration.GetConnectionString(Constraints.constraints.getConnection(conn.Trim()));
                this.OpenConnection();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        //----------------------------//-----------------------------//
        public Boolean ConfirmTransaction()
        {
            try
            {
                transaction.Commit();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        //----------------------------//-----------------------------//
        public void CloseConnection()
        {
            if (!(connection == null))
            {
                connection.Close();
            }

            if (!(transaction == null))
            {
                transaction.Dispose();
            }
        }

        //----------------------------//-----------------------------//
        public void OpenConnection()
        {
            if (connection == null)
            {
                connection = new SqlConnection(this.connectionString);
                connection.Open();

                if (transaction == null)
                {
                    transaction = connection.BeginTransaction("TransacaoGeral");
                }
            }
            else if (connection.State == System.Data.ConnectionState.Closed)
            {
                connection.Open();
                transaction = connection.BeginTransaction("TransacaoGeral");
            }
        }

        //-- executa procedure tipo: Inserir com retorno, Alterar com retorno, Excluir com retorno e SELECT
        public DataSet ExecuteDataReader(string cmdText, CommandType cmdType, SqlParameter[] parameters)
        {
            DataSet dts = new DataSet();
            SqlCommand cmd = CreateCommand(cmdText, cmdType, parameters);
            SqlDataAdapter objAdapter = new SqlDataAdapter(cmd);

            try
            {
                objAdapter.Fill(dts);
                return dts;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                objAdapter = null;
            }
        }

        public SqlCommand CreateCommand(string cmdText, CommandType cmdType, SqlParameter[] parameters)
        {
            SqlCommand cmd = new SqlCommand(cmdText);

            cmd.CommandType = cmdType;
            cmd.Connection = connection;
            cmd.Transaction = transaction;

            if ((parameters != null))
            {
                foreach (SqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
            }
            return cmd;
        }

        //----------------------------//-----------------------------//        
        public SqlParameter[] CreateArrayParameter(Object[,] parameters)
        {
            SqlParameter param = null;
            SqlParameter[] paramArray = new SqlParameter[parameters.GetLength(0)];

            // percorre array parameters e adiciona nome e valores dos parametros no array paramArray
            for (int i = 0; i < parameters.GetLength(0); i++)
            {
                param = new SqlParameter();
                param.ParameterName = parameters[i, 0].ToString();

                if (parameters[i, 1] == null)
                { // valor do parametro
                    param.Value = DBNull.Value;
                }
                else
                {
                    param.Value = parameters[i, 1];
                }

                paramArray[i] = param;
            }

            return paramArray;
        }
    }




}