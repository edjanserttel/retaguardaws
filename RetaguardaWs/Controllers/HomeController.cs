﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RetaguardaWs.Business;
using RetaguardaWs.Models;
using RetaguardaWs.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Controllers
{

    public class HomeController : Controller
    {
        [Route("[controller]/Login")]
        [HttpPost]
        public DataSet Login([FromBody]User user)
        {
            if (CamposValidos(user))
            {
                try
                {
                    DataSet dts = new BCUser().Login(user);
                    return dts;
                }
                catch (Exception ex)
                {
                }
            }
            return null;
        }

        [Route("[controller]/GetAllProject")]
        [HttpPost]
        public DataSet GetAllProject()
        {
            try
            {
                DataSet dts = new BCProject().GetAllProjects();
                return dts;
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        private bool CamposValidos(User user)
        {
            return !string.IsNullOrEmpty(user.Project) && !string.IsNullOrEmpty(user.Password) && !string.IsNullOrEmpty(user.Login) && !user.Project.Equals("0");
        }
    }
}