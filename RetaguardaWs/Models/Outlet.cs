﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class Outlet
    {
        public int ID { get; set; }
        [Display(Name = "Nome fantasia")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo NOME FANTASIA deve ser preenchido")]
        public string Nickname { get; set; }

        [Display(Name = "Razão social")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo RAZÃO SOCIAL deve ser preenchido")]
        public string Name { get; set; }

        [Display(Name = "CPF/CNPJ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo CPF/CNPJ deve ser preenchido")]
        [StringLength(14)]
        public string CPF { get; set; }

        [Display(Name = "Bloqueado")]
        [Range(0, 1)]
        [Required(ErrorMessage = "O campo BLOQUEADO deve ser preenchido")]
        public int Blocked { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }

        [Display(Name = "Atividade")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo ATIVIDADE deve ser preenchido")]
        public string Activity { get; set; }

        [Display(Name = "Horário de funcionamento")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo HORÁRIO DE FUNCIONAMENTO deve ser preenchido")]
        public string FuncHour { get; set; }

        [Display(Name = "Responsável")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo RESPONSÁVEL deve ser preenchido")]
        public string Sponsor { get; set; }

        [Display(Name = "Observações")]
        public string Obs { get; set; }

        [Display(Name = "Número de série")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo NÚMERO DE SÉRIE deve ser preenchido")]
        public string SerialNumber { get; set; }

        [Display(Name = "Versão")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo VERSÃO deve ser preenchido")]
        public string Version { get; set; }

        [Display(Name = "Código credenciadora")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo CÓDIGO CREDENCIADORA deve ser preenchido")]
        public string CodeCreden { get; set; }

        [Display(Name = "Email")]
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Informe um endereço de e-mail válido")]
        public string UserEmail { get; set; }

        [Display(Name = "Senha")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo SENHA deve ser preenchido")]
        public string UserPassword { get; set; }

        public string UserGlobalId { get; set; }

        [Display(Name = "Logradouro")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo LOGRADOURO deve ser preenchido")]
        public string AddressStreet { get; set; }

        [Display(Name = "Número")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo NÚMERO deve ser preenchido")]
        public string AddressNumber { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "O campo CEP deve ser preenchido corretamente", MinimumLength = 10)]
        public string CEP { get; set; }

        public List<string> PhoneNumbers { get; set; } = new List<string>();

        [Display(Name = "Saldo")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo SALDO deve ser preenchido")]
        public string Balance { get; set; }

        [Display(Name = "Tipo")]
        [Required(ErrorMessage = "O campo TIPO deve ser preenchido")]
        public int Type { get; set; }

        [Display(Name = "Imagem")]
        [Required(ErrorMessage = "Por favor, selecione a imagem do PDV")]
        public string Image { get; set; }

        [Display(Name = "Percentual de descontos")]
        [Required(ErrorMessage = "O campo PERCENTUAL deve ser preenchido")]
        public string Percentual { get; set; }

        public bool isValid()
        {
            if (
                !string.IsNullOrEmpty(this.Nickname) &&
                !string.IsNullOrEmpty(this.Name) &&
                validCpf(this.CPF) &&
                !string.IsNullOrEmpty(this.Image) &&
                !string.IsNullOrEmpty(this.Percentual) &&
                !string.IsNullOrEmpty(this.Balance) &&
                validCEP(this.CEP) &&
                !string.IsNullOrEmpty(this.AddressNumber) &&
                !string.IsNullOrEmpty(this.AddressStreet) &&
                !string.IsNullOrEmpty(this.UserPassword) &&
                !string.IsNullOrEmpty(this.UserEmail) &&
                !string.IsNullOrEmpty(this.CodeCreden) &&
                !string.IsNullOrEmpty(this.Version) &&
                !string.IsNullOrEmpty(this.SerialNumber) &&
                !string.IsNullOrEmpty(this.Sponsor) &&
                !string.IsNullOrEmpty(this.FuncHour) &&
                !string.IsNullOrEmpty(this.Activity) &&
                this.PhoneNumbers.Count > 0
                )
            {
                return true;
            }
            return false;
        }

        private bool validCEP(string CEP)
        {
            CEP = CEP.Replace(".", "").Replace("-", "").Replace("/", "");
            return !string.IsNullOrEmpty(CEP) && CEP.Length == 8 ? true : false;
        }

        private bool validCpf(string CPF)
        {
            CPF = CPF.Replace(".", "").Replace("-", "").Replace("/", "");
            return CPF.Length == 11 || CPF.Length == 14 ? true : false;
        }
    }
}