﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class Menu
    {
        public int CodMenu { get; set; }
        public string Description { get; set; }
        public int Parent { get; set; }
        public int Order { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Icon { get; set; }
        public string CssClass { get; set; }
    }
}