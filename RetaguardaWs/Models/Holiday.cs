﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class Holiday
    {
        public long Id { get; set; }

        [Required]
        [Display(Name = "Data")]
        [Range(typeof(DateTime), "1/1/1901", "1/1/2100",
            ErrorMessage = "Informe uma data válida")]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Descrição")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Situação")]
        public int Blocked { get; set; }

        public bool isValid()
        {
            if (
                !string.IsNullOrEmpty(this.Description) &&
                validDate()
            ){
                return true;
            }
            return false;
        }
        public bool validDate()
        {
            if ((Date.CompareTo(DateTime.Parse("1/1/1901")) >= 0) && (Date.CompareTo(DateTime.Now) <= 0))
            {
                return true;
            }
            return false;
        }
    }
}