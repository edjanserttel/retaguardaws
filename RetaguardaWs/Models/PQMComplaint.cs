﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class PQMComplaint
    {
        public long Id { get; set; }

        public long IdUser { get; set; }

        [Display(Name = "Data da reclamação")]
        [Required]
        public string DateComplaint { get; set; }

        [Display(Name = "Hora da reclamação")]
        [Required]
        public string HourComplaint { get; set; }

        [Display(Name = "Parquímetro")]
        [Required]
        public long IdPQM { get; set; }

        [Display(Name = "Reclamente")]
        [Required]
        public string Claimant { get; set; }

        #region Problema
        [Display(Name = "Problema")]
        [Required]
        public long IdProblem { get; set; }

        public string DescProblem { get; set; }

        [Display(Name = "Complemento do problema")]
        [Required]
        public string CompProblem { get; set; }

        [Display(Name = "Problema real")]
        [Required]
        public string RealProblem { get; set; }
        #endregion

        #region Ação
        [Display(Name = "Data da ação")]
        [Required]
        public string DateAction { get; set; }

        [Display(Name = "Hora da ação")]
        [Required]
        public string HourAction { get; set; }

        [Display(Name = "Ação")]
        [Required]
        public long IdAction { get; set; }
        
        [Display(Name = "Complemento da ação")]
        [Required]
        public string DescAction { get; set; }

        [Display(Name = "Responsável pela ação")]
        [Required]
        public string ResponsibleAction { get; set; }
        #endregion

        [Display(Name = "Motivo do cancelamento")]
        [Required]
        public string ReasonCancel { get; set; }

        [Display(Name = "Situação")]
        [Required]
        public int Blocked { get; set; }
    }
}