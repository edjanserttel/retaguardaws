﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class Cash
    {
        public long Id { get; set; }

        [Display(Name = "Tipo")]
        [Required]
        public int Type { get; set; }

        [Display(Name = "Responsável")]
        [Required]
        public long IdSponsor { get; set; }

        public string NameSponsor { get; set; }

        [Display(Name = "Valor de abertura")]
        [Required]
        public string ApertureValue { get; set; }

        [Display(Name = "Limite")]
        [Required]
        public string Limit { get; set; }

        [Display(Name = "Percentual de desconto %")]
        [Required]
        public string Percentual { get; set; }

        [Display(Name = "Data e hora abertura")]
        [Required]
        public string OpenDate { get; set; }

        [Display(Name = "Data e hora fechamento")]
        [Required]
        public string CloseDate { get; set; }

        [Display(Name = "Bloqueado")]
        [Required]
        public int Blocked { get; set; }

        public bool isValidOutlet()
        {
            if (!string.IsNullOrEmpty(this.Limit))
            {
                return true;
            }
            return false;
        }

        public bool isValidSupervisor()
        {
            try
            {
                if (
                        !string.IsNullOrEmpty(this.Limit) &&
                        !string.IsNullOrEmpty(this.ApertureValue) &&
                        int.Parse(this.Percentual.Replace("%", "")) > 0
                        )
                {
                    return true;
                }
            }
            catch (Exception)
            {
            }
            return false;
        }
    }
}