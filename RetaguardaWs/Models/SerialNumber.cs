﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class SerialNumber
    {
        public long NumberSerialId { get; set; }

        [Required]
        [Display(Name = "Descrição")]
        public String Description { get; set; }

        [Required]
        [Display(Name = "Número de Lote")]
        public int GroupNumber { get; set; }
        
        [Required]
        [Display(Name = "Número de Série")]
        public String Series { get; set; }

        [Required]
        [Display(Name = "Número de Tíquete")]
        public int NumberInitialCard { get; set; }

        [Required]
        [Display(Name = "Número de Tíquete")]
        public int NumberFinalCard { get; set; }

        [Required]
        [Display(Name = "Cancelado")]
        public int Blocked { get; set; }

        public bool isValid() {
            if (!string.IsNullOrEmpty(this.Description) &&
                !string.IsNullOrEmpty(this.Series) &&
                GroupNumber > 0 &&
                NumberInitialCard > 0 &&
                NumberFinalCard > 0
                )
            {
                return true;
            }
            return false;
        }
    }
}