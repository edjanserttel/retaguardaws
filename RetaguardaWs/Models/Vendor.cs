﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class Vendor
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}