﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace RetaguardaWs.Models
{
    public class OfficialVehicle
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Informe um valor válido para o campo Responsável")]
        [Display(Name = "Responsável")]
        public String Responsible { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Informe um valor válido para o campo Placa")]
        [Display(Name = "Placa")]
        [RegularExpression("^[a-zA-Z]{3}[0-9]{4}$",
            ErrorMessage = "A Placa não está no formato correto")]
        public String LicensePlate { get; set; }

        [Required]
        [Range(typeof(DateTime), "1/1/1901", "1/1/2100",
            ErrorMessage = "Informe uma data válida")]
        [Display(Name = "Data")]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Situação")]
        public int Blocked { get; set; }



        public bool isValid()
        {
            if (
                !string.IsNullOrEmpty(this.Responsible) &&
                !string.IsNullOrEmpty(this.LicensePlate) &&
                Util.UtilFunctions.util.MatchPlate(this.LicensePlate)
                ){
                return true;
            }

            return false;
        }
    }
}