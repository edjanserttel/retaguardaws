﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class Subsector
    {
        public long Id { get; set; }
        [Required]
        [Display(Name = "Descrição")]
        public string Desc { get; set; }

        [Required]
        [Display(Name = "Setor")]
        public long IdSector { get; set; }

        public string DescSector { get; set; }

        [Required]
        [Display(Name = "Referência")]
        public string Reference { get; set; }

        [Required]
        [Display(Name = "Endereço")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Vagas carro")]
        public int ParkingLotCar { get; set; }

        [Required]
        [Display(Name = "Vagas moto")]
        public int ParkingLotMoto { get; set; }

        [Required]
        [Display(Name = "Vagas idoso")]
        public int ParkingLotAncient { get; set; }

        [Required]
        [Display(Name = "Vagas deficiente")]
        public int ParkingLotDeficient { get; set; }

        [Required]
        [Display(Name = "Latitude inicial")]
        public string InitialLatitude { get; set; }

        [Required]
        [Display(Name = "Longitude inicial")]
        public string InitialLongitude { get; set; }

        [Required]
        [Display(Name = "Latitude final")]
        public string FinalLatitude { get; set; }

        [Required]
        [Display(Name = "Longitude final")]
        public string FinalLongitude { get; set; }

        [Required]
        [Display(Name = "Bloqueado")]
        public int Blocked { get; set; }
    }
}