﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class Project
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}