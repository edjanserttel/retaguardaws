﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class PQM
    {
        public long Id { get; set; }

        [Required]
        [Display(Name = "Número")]
        public string Number { get; set; }

        [Required]
        [Display(Name = "Id subsetor")]
        public long IdSubSector { get; set; }

        public long IdSector { get; set; }

        public string SubSetor { get; set; }

        public int ParkingLots { get; set; }

        [Required]
        [Display(Name = "Longitude")]
        public string Longitude { get; set; }

        [Required]
        [Display(Name = "Latitude")]
        public string Latitude { get; set; }

        [Display(Name = "Referência")]
        public string Local { get; set; }

        [Display(Name = "Modelo")]
        public string Model { get; set; }

        public string Firmware { get; set; }

        [Display(Name = "Bloqueado")]
        public int Blocked { get; set; }
    }
}