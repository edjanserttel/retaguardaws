﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class Equipment
    {
        public long Id { get; set; }
        [Display(Name = "Responsável associado")]
        [Required]
        public long IdPdv { get; set; }

        [Display(Name = "Nome")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(20)]
        public string Name { get; set; }

        [Display(Name = "Observação")]
        public string Obs { get; set; }

        [Display(Name = "Tipo")]
        [Required]
        public short Type { get; set; }
        public string TypeDescription { get; set; }

        [Display(Name = "Modelo")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Model { get; set; }

        [Display(Name = "IMEI")]
        public string IMEI { get; set; }

        [Display(Name = "MAC")]
        public string MAC { get; set; }

        [Display(Name = "Operadora do chip")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string SIMTel { get; set; }

        [Display(Name = "Número de identificação do chip")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string SIMNumber { get; set; }

        [Display(Name = "Número")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string SIMNumberLine { get; set; }

        [Display(Name = "Versão do firmware")]
        public string Firmware { get; set; }

        [Display(Name = "Bloqueado")]
        [Required]
        public int Blocked { get; set; }
    }
}