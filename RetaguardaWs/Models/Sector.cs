﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class Sector
    {
        public long Id { get; set; }

        [Required]
        [Display(Name = "Descrição")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Referência")]
        public string Reference { get; set; }

        [Required]
        [Display(Name = "Bloqueado")]
        public int Blocked { get; set; }
    }
}