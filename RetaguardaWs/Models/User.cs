﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class User
    {
        public long Id { get; set; }
        [Required()]
        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Required()]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required()]
        [Display(Name = "Login/Matrícula")]
        public string Login { get; set; }

        [Required()]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        [Required()]
        [Display(Name = "Tipo de usuário")]
        public int TypeUser { get; set; }

        public string TypeUserDescription { get; set; }

        [Required()]
        [Display(Name = "Bloqueado")]
        public int Blocked { get; set; }
                
        [Required]
        public string Project { get; set; }
    }
}