﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Models
{
    public class Occurrence
    {
        public long Id { get; set; }
        [Display(Name = "Descrição")]
        [Required]
        public string Description { get; set; }

        [Display(Name = "Descrição Técnica")]
        [Required]
        public string TechnicalDescription { get; set; }

        [Display(Name = "Tipo")]
        [Required]
        public int Type { get; set; }

        [Display(Name = "Bloqueado")]
        [Required]
        public int Blocked { get; set; }
    }
}