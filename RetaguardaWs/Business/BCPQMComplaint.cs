﻿using RetaguardaWs.Database;
using RetaguardaWs.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Business
{
    public class BCPQMComplaint
    {
        public DataSet GetAllPQMComplaint(string id, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[1, 2] {
                                                                        {"@Id", id }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_REC_PQMOcorrencia", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet CreatePQMComplaint(PQMComplaint complaint, string idUser, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[6, 2] {
                                                                        {"@IdPQM", complaint.IdPQM },
                                                                        {"@IdProblema", complaint.IdProblem },
                                                                        {"@IdUsuario", idUser },
                                                                        {"@Responsavel", complaint.Claimant },
                                                                        {"@DataHoraReclamacao", DateTime.Parse(complaint.DateAction + complaint.HourAction).ToString("MM/dd/yyyy HH:mm") },
                                                                        {"@ComplementoProblema", complaint.CompProblem }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_INSERIR_PQMOcorrencia", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet EditPQMComplaint(PQMComplaint complaint, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[8, 2] {
                                                                        {"@Id", complaint.Id },
                                                                        {"@ResponsavelAcao", complaint.ResponsibleAction },
                                                                        {"@DataHoraAcao", DateTime.Parse(complaint.DateAction + complaint.HourAction).ToString("MM/dd/yyyy HH:mm") },
                                                                        {"@ProblemaReal", complaint.RealProblem },
                                                                        {"@IdAcao", complaint.IdAction },
                                                                        {"@DescAcao", complaint.DescAction },
                                                                        {"@MotivoCancelar", complaint.ReasonCancel },
                                                                        {"@Situacao", complaint.Blocked }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_PQMOcorrencia", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }
    }
}