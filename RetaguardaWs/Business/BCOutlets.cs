﻿using RetaguardaWs.Database;
using RetaguardaWs.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Business
{
    public class BCOutlets
    {
        public DataSet GetAllOutlets(string id, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[1, 2] {
                                                                        {"@CodPontoVenda", id }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_REC_PontoVenda", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet UpdateOutlet(Outlet outlet, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                string phoneNumbers = "";
                foreach (var item in outlet.PhoneNumbers)
                {
                    phoneNumbers += item + " | ";
                }
                //Atualiza a lista de telefones daquele PDV
                UpdatePhoneNumbers(outlet.ID, phoneNumbers, project);

                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[21, 2] {
                                                                        {"@Id", outlet.ID },
                                                                        {"@RzSocial", outlet.Name },
                                                                        {"@NomeFantasia", outlet.Nickname },
                                                                        {"@CpfCnpj", Util.UtilFunctions.util.RemoveMask(outlet.CPF) },
                                                                        {"@Bloqueado", outlet.Blocked },
                                                                        {"@Latitude", outlet.Latitude },
                                                                        {"@Longitude", outlet.Longitude },
                                                                        {"@Tipo", outlet.Type },
                                                                        {"@Atividade", outlet.Activity },
                                                                        {"@HorarioFunc", outlet.FuncHour },
                                                                        {"@Obs", outlet.Obs },
                                                                        {"@NumSerieSerttel", outlet.SerialNumber },
                                                                        {"@Versao", outlet.Version },
                                                                        {"@Responsavel", outlet.Sponsor },
                                                                        {"@UsuarioEmail", outlet.UserEmail },
                                                                        {"@UsuarioSenha", outlet.UserPassword },
                                                                        {"@CEP", Util.UtilFunctions.util.RemoveMask(outlet.CEP) },
                                                                        {"@Logradouro", outlet.AddressStreet },
                                                                        {"@Numero", outlet.AddressNumber },
                                                                        {"@Percentual", outlet.Percentual.Replace(",", ".") },
                                                                        {"@Imagem", outlet.Image }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_PontoVenda", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet BlockOutlet(string id, int blocked, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[2, 2] {
                                                                        {"@idPdv", id },
                                                                        {"@Bloquear", blocked == 0 ? 1 : 0}
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_BloquearPDV", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet InsertOutlet(Outlet outlet, string user, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[22, 2] {
                                                                        {"@RzSocial", outlet.Name },
                                                                        {"@NomeFantasia", outlet.Nickname },
                                                                        {"@CpfCnpj", Util.UtilFunctions.util.RemoveMask(outlet.CPF) },
                                                                        {"@Bloqueado", outlet.Blocked },
                                                                        {"@Latitude", outlet.Latitude },
                                                                        {"@Longitude", outlet.Longitude },
                                                                        {"@Tipo", outlet.Type },
                                                                        {"@Atividade", outlet.Activity },
                                                                        {"@HorarioFunc", outlet.FuncHour },
                                                                        {"@Obs", outlet.Obs },
                                                                        {"@NumSerieSerttel", outlet.SerialNumber },
                                                                        {"@Versao", outlet.Version },
                                                                        {"@Responsavel", outlet.Sponsor },
                                                                        {"@UsuarioEmail", outlet.UserEmail },
                                                                        {"@UsuarioSenha", outlet.UserPassword },
                                                                        {"@CEP", Util.UtilFunctions.util.RemoveMask(outlet.CEP) },
                                                                        {"@Logradouro", outlet.AddressStreet },
                                                                        {"@Numero", outlet.AddressNumber },
                                                                        {"@CadCred", outlet.CodeCreden},
                                                                        {"@UsuarioCadastro", user },
                                                                        {"@Percentual", outlet.Percentual.Replace(",", ".") },
                                                                        {"@Imagem", outlet.Image }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_INSERIR_PontoVenda", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();

                string phoneNumbers = "";
                foreach (var item in outlet.PhoneNumbers)
                {
                    phoneNumbers += item + " | ";
                }
                //Cadastra os telefones
                UpdatePhoneNumbers(Convert.ToInt32(dts.Tables[0].Rows[0]["Id"].ToString()), phoneNumbers, project);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        private void UpdatePhoneNumbers(int ID, string phoneNumbers, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[2, 2] {
                                                                        {"@IdPdv", ID },
                                                                        {"@PhoneNumbers", phoneNumbers }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_INSERIR_PontoVendaTelefone", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }
        }
    }
}