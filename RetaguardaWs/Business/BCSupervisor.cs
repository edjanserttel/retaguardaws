﻿using RetaguardaWs.Database;
using RetaguardaWs.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Business
{
    public class BCSupervisor
    {
        public DataSet GetSupervisor(string id, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[1, 2] {
                                                                        {"@Id", id }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_REC_Supervisor", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet GetSupervisorMonitor(string id, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[1, 2] {
                                                                        {"@Id", id }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_REC_SupervisorMonitor", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet CreateSupervisor(User user, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[6, 2] {
                                                                        {"@Login", user.Login},
                                                                        {"@Email", user.Email},
                                                                        {"@Nome", user.Name},
                                                                        {"@Senha", user.Password},
                                                                        {"@TipoUsuario", user.TypeUser },
                                                                        {"@Bloqueado", user.Blocked }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_INSERIR_Supervisor", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet EditSupervisor(User user, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[7, 2] {
                                                                        {"@Id", user.Id },
                                                                        {"@Login", user.Login },
                                                                        {"@Email", user.Email },
                                                                        {"@Nome", user.Name },
                                                                        {"@Senha", user.Password },
                                                                        {"@TipoUsuario", user.TypeUser },
                                                                        {"@Bloqueado", user.Blocked }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_Supervisor", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet BlockSupervisor(string id, int blocked, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[2, 2] {
                                                                        {"@Id", id },
                                                                        {"@Bloqueado", blocked == 0 ? 1 : 0 },
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_BloquearSupervisor", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }
    }
}