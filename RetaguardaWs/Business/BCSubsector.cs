﻿using RetaguardaWs.Database;
using RetaguardaWs.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Business
{
    public class BCSubsector
    {
        public DataSet GetAllSubsector(string idSector, string idSubsector, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[2, 2] {
                                                                        {"@IdSetor", idSector },
                                                                        {"@IdSubsetor", idSubsector }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_REC_SetorSubsetores", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet GetSubsector(string id, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[1, 2] {
                                                                        {"@Id", id }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_REC_Subsetor", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        internal DataSet CreateSubsector(Subsector subsector, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[13, 2] {
                                                                        {"@DescSubSetor", subsector.Desc },
                                                                        {"@IdSetor", subsector.IdSector },
                                                                        {"@Referencia", subsector.Reference },
                                                                        {"@Endereco", subsector.Address },
                                                                        {"@VagasCarro", subsector.ParkingLotCar },
                                                                        {"@VagasMoto", subsector.ParkingLotMoto },
                                                                        {"@VagasDef", subsector.ParkingLotDeficient },
                                                                        {"@VagasIdoso", subsector.ParkingLotAncient },
                                                                        {"@LatitudeIni", subsector.InitialLatitude },
                                                                        {"@LongitudeIni", subsector.InitialLongitude },
                                                                        {"@LatitudeFin", subsector.FinalLatitude },
                                                                        {"@LongitudeFin", subsector.FinalLongitude },
                                                                        {"@Bloqueado", subsector.Blocked },
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_INSERIR_Subsetor", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        internal DataSet EditSubsector(Subsector subsector, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[14, 2] {
                                                                        {"@Id", subsector.Id },
                                                                        {"@DescSubSetor", subsector.Desc },
                                                                        {"@IdSetor", subsector.IdSector },
                                                                        {"@Referencia", subsector.Reference },
                                                                        {"@Endereco", subsector.Address },
                                                                        {"@VagasCarro", subsector.ParkingLotCar },
                                                                        {"@VagasMoto", subsector.ParkingLotMoto },
                                                                        {"@VagasDef", subsector.ParkingLotDeficient },
                                                                        {"@VagasIdoso", subsector.ParkingLotAncient },
                                                                        {"@LatitudeIni", subsector.InitialLatitude },
                                                                        {"@LongitudeIni", subsector.InitialLongitude },
                                                                        {"@LatitudeFin", subsector.FinalLatitude },
                                                                        {"@LongitudeFin", subsector.FinalLongitude },
                                                                        {"@Bloqueado", subsector.Blocked },
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_Subsetor", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        internal DataSet BlockSubsector(string id, int blocked, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[2, 2] {
                                                                        {"@Id", id },
                                                                        {"@Bloqueado", blocked == 0 ? 1: 0 },
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_BloquearSubsetor", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }
    }
}