﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using RetaguardaWs.Database;
using RetaguardaWs.Models;

namespace RetaguardaWs.Business
{
    public class BCUser
    {
        public DataSet Login(User user)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(user.Project);

                Object[,] arrayObjetoParametros = new Object[2, 2] {
                                                                        {"@Login", user.Login },
                                                                        {"@Senha", user.Password }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_AutenticarUsuario", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }
    }
}