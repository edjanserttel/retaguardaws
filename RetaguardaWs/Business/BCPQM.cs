﻿using RetaguardaWs.Database;
using RetaguardaWs.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Business
{
    public class BCPQM
    {
        public DataSet GetAllPQM(string id, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[1, 2] {
                                                                        {"@Id", id }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_REC_PQM", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet CreatePQM(PQM pqm, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[8, 2] {
                                                                        {"@Numero", pqm.Number },
                                                                        {"@IdSubSetor", pqm.IdSubSector },
                                                                        {"@Longitude", pqm.Longitude },
                                                                        {"@Latitude", pqm.Latitude },
                                                                        {"@LocalRef", pqm.Local },
                                                                        {"@Modelo", pqm.Model },
                                                                        {"@Firmware", pqm.Firmware },
                                                                        {"@Bloqueado", pqm.Blocked }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_INSERIR_PQM", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction(); 
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        internal DataSet BlockPQM(string id, int blocked, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[2, 2] {
                                                                        {"@Id", id },
                                                                        {"@Bloquear", blocked == 0 ? 1 : 0}
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_BloquearPQM", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet UpdatePQM(PQM pqm, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[9, 2] {
                                                                        {"@Id", pqm.Id },
                                                                        {"@Numero", pqm.Number },
                                                                        {"@IdSubSetor", pqm.IdSubSector },
                                                                        {"@Longitude", pqm.Longitude },
                                                                        {"@Latitude", pqm.Latitude },
                                                                        {"@LocalRef", pqm.Local },
                                                                        {"@Modelo", pqm.Model },
                                                                        {"@Firmware", pqm.Firmware },
                                                                        {"@Bloqueado", pqm.Blocked }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_PQM", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }
    }
}