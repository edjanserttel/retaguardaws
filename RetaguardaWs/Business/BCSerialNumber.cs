﻿using RetaguardaWs.Database;
using RetaguardaWs.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Business
{
    public class BCSerialNumber
    {
        public DataSet GetAllSerialNumbers(long id, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[1, 2] {
                                                                {"@Id", id }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_REC_NumeroSerie", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        //finalizado
        public DataSet CreateSerialNumber(SerialNumber serialNumber, long codUsuario, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[7, 2] {
                                                                        {"@CodUsuario", codUsuario},
                                                                        {"@DescNumSerie", serialNumber.Description},
                                                                        {"@Numtalao", serialNumber.GroupNumber},
                                                                        {"@Serie", serialNumber.Series.ToString()},
                                                                        {"@NumCartaoInicial", serialNumber.NumberInitialCard},
                                                                        {"@NumCartaoFinal", serialNumber.NumberFinalCard},
                                                                        {"@Bloqueado", serialNumber.Blocked}
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_INSERIR_NumeroSerie", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet EditSerialNumber(SerialNumber serialNumber, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[7, 2] {
                                                                        {"@CodNumSerie", serialNumber.NumberSerialId},
                                                                        {"@DescNumSerie", serialNumber.Description},
                                                                        {"@Numtalao", serialNumber.GroupNumber},
                                                                        {"@Serie", serialNumber.Series},
                                                                        {"@NumCartaoInicial", serialNumber.NumberInitialCard},
                                                                        {"@NumCartaoFinal", serialNumber.NumberFinalCard},
                                                                        {"@Bloqueado", serialNumber.Blocked}
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_NumeroSerie", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet BlockSerialNumber(long id, int blocked, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[2, 2] {
                                                                        {"@CodNumSerie", id },
                                                                        {"@Bloqueado", blocked == 0 ? 1 : 0 },
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_BloquearNumeroSerie", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }
    }
}