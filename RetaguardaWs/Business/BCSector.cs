﻿using RetaguardaWs.Database;
using RetaguardaWs.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Business
{
    public class BCSector
    {
        public DataSet GetAllSector(string id, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[1, 2] {
                                                                        {"@Id", id }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_REC_Setor", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet CreateSector(Sector sector, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[3, 2] {
                                                                        {"@DescSetor", sector.Description },
                                                                        {"@RefSetor", sector.Reference },
                                                                        {"@Bloqueado", sector.Blocked }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_INSERIR_Setor", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet EditSector(Sector sector, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[4, 2] {
                                                                        {"@Id", sector.Id },
                                                                        {"@DescSetor", sector.Description },
                                                                        {"@RefSetor", sector.Reference },
                                                                        {"@Bloqueado", sector.Blocked }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_Setor", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet BlockSector(string id, int blocked, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[2, 2] {
                                                                        {"@Id", id },
                                                                        {"@Bloqueado", blocked == 0 ? 1 : 0 },
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_BloquearSetor", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

    }
}