﻿using RetaguardaWs.Database;
using RetaguardaWs.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Business
{
    public class BCEquipment
    {
        public DataSet GetAllEquipaments(string id, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[1, 2] {
                                                                        {"@Id", id }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_REC_Equipamento", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet GetAllTypes(string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                parametro = connection.CreateArrayParameter(new Object[0, 0]);
                dts = connection.ExecuteDataReader("RET_REC_TipoEquipamento", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet UpdateEquipament(Equipment equipament, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[13, 2] {
                                                                        {"@Id", equipament.Id },
                                                                        {"@IdPdv", equipament.IdPdv },
                                                                        {"@Nome", equipament.Name },
                                                                        {"@Obs", equipament.Obs },
                                                                        {"@Tipo", equipament.Type },
                                                                        {"@Modelo", equipament.Model },
                                                                        {"@IMEI", equipament.IMEI },
                                                                        {"@MAC", equipament.MAC },
                                                                        {"@ChipOP", equipament.SIMTel },
                                                                        {"@ChipNum", equipament.SIMNumber },
                                                                        {"@ChipNumL", equipament.SIMNumberLine },
                                                                        {"@Firmware", equipament.Firmware },
                                                                        {"@Bloqueado", equipament.Blocked }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_Equipamento", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet GetAllSponsor(string tipoEquipamento, string responsavel, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[2, 2] {
                                                                        {"@TipoEquipamento", tipoEquipamento },
                                                                        {"@Responsavel", responsavel }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_REC_EquipamentoResponsavel", CommandType.StoredProcedure, parametro);
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet BlockEquipament(string id, int blocked, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[2, 2] {
                                                                        {"@Id", id },
                                                                        {"@Bloquear", blocked == 0 ? 1 : 0}
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_ATUALIZAR_BloquearEquipamento", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }

        public DataSet InsertEquipament(Equipment equipament, string project)
        {
            SQLServer connection = null;
            DataSet dts = null;
            SqlParameter[] parametro = null;

            try
            {
                connection = new SQLServer(project);

                Object[,] arrayObjetoParametros = new Object[12, 2] {
                                                                        {"@IdPdv", equipament.IdPdv },
                                                                        {"@Nome", equipament.Name },
                                                                        {"@Obs", equipament.Obs },
                                                                        {"@Tipo", equipament.Type },
                                                                        {"@Modelo", equipament.Model },
                                                                        {"@IMEI", equipament.IMEI },
                                                                        {"@MAC", equipament.MAC },
                                                                        {"@ChipOP", equipament.SIMTel },
                                                                        {"@ChipNum", equipament.SIMNumber },
                                                                        {"@ChipNumL", equipament.SIMNumberLine },
                                                                        {"@Firmware", equipament.Firmware },
                                                                        {"@Bloqueado", equipament.Blocked }
                };

                parametro = connection.CreateArrayParameter(arrayObjetoParametros);
                dts = connection.ExecuteDataReader("RET_INSERIR_Equipamento", CommandType.StoredProcedure, parametro);
                connection.ConfirmTransaction();
            }
            catch (Exception e)
            {
                dts = null;
            }
            finally
            {
                if (connection != null)
                    connection.CloseConnection();
            }

            return dts;
        }
    }
}