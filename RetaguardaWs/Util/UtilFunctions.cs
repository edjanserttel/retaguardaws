﻿using RetaguardaWs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace RetaguardaWs.Util
{
    public class UtilFunctions
    {
        private UtilFunctions() { }
        public static UtilFunctions util { get; } = new UtilFunctions();

        public string MaskCPFCNPJ(string cpfcnpj)
        {
            cpfcnpj = RemoveMask(cpfcnpj);
            return cpfcnpj.Length > 11 ? Convert.ToUInt64(cpfcnpj).ToString(@"00\.000\.000\/0000\-00") : Convert.ToUInt64(cpfcnpj).ToString(@"000\.000\.000\-00");
        }
        public string RemoveMask(string cpfcnpj)
        {
            return cpfcnpj.Replace(".", string.Empty).Replace("-", string.Empty).Replace("/", string.Empty);
        }

        public bool MatchPlate(string plate) {
            Regex regexPlaca = new Regex(@"^[a-zA-Z]{3}[0-9]{4}$");
            return regexPlaca.IsMatch(plate.Replace("-", ""));  
        }
    }
}