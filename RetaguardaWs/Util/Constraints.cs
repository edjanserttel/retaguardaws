﻿using RetaguardaWs.Business;
using RetaguardaWs.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace RetaguardaWs.Util
{
    public class Constraints
    {
        private Constraints() { }

        public string getConnection(string project)
        {
            return connections[project];
        }
        public List<Project> getAllProjects()
        {
            return projects;
        }

        public Project GetProject(string code)
        {
            return projects.Find(p => p.Code.Trim() == code);
        }
        public static Constraints constraints { get; } = new Constraints();

        private List<Project> projects = new List<Project>();
        private Dictionary<string, string> connections = new Dictionary<string, string>();

        private const string DB_SAOJOSE = "6";
        private const string DB_CARUARU = "7";
        private const string DB_ARAUCARIA = "8";
        private const string DB_PETROLINA = "9";
        private const string DB_LAVRAS = "12";
        private const string DB_GRU = "13";
        private const string DB_CET = "18";
        private const string DB_PELOTAS = "23";
        private const string DB_FORTALEZA = "26";
        private const string DB_CARAGUA = "27";
        private const string DB_CRATO = "29";
        private const string DB_RIOBRANCO = "37";
        private const string DB_ARACAJU = "45";
        private const string DB_CJORDAO = "48";
        private const string DB_PESQUEIRA = "67";
        private const string DB_SERRA = "76";
        private const string DB_BHTRANS = "97";
        private const string DB_DIADEMA = "102";
        private const string DB_UNICO = "500";

        public void LoadConnections()
        {
            connections.Add(DB_SAOJOSE, "ConnSQLServerSjc");
            connections.Add(DB_CARUARU, "ConnSQLServerCaruaru");
            connections.Add(DB_ARAUCARIA, "ConnSQLServerAraucaria");
            connections.Add(DB_PETROLINA, "ConnSQLServerPetrolina");
            connections.Add(DB_LAVRAS, "ConnSQLServerLavras");
            connections.Add(DB_GRU, "ConnSQLServerGRU");
            connections.Add(DB_CET, "ConnSQLServerCET");
            connections.Add(DB_PELOTAS, "ConnSQLServerPelotas");
            connections.Add(DB_FORTALEZA, "ConnSQLServerFortaleza");
            connections.Add(DB_CARAGUA, "ConnSQLServerCaragua");
            connections.Add(DB_CRATO, "ConnSQLServerCrato");
            connections.Add(DB_RIOBRANCO, "ConnSQLServerRioBranco");
            connections.Add(DB_ARACAJU, "ConnSQLServerAracaju");
            connections.Add(DB_CJORDAO, "ConnSQLServerCJordao");
            connections.Add(DB_PESQUEIRA, "ConnSQLServerPesqueira");
            connections.Add(DB_SERRA, "ConnSQLServerSerra");
            connections.Add(DB_BHTRANS, "ConnSQLServerBHTrans");
            connections.Add(DB_DIADEMA, "ConnSQLServerDiadema");
            connections.Add(DB_UNICO, "ConnSQLServerUnico");
        }

        public void LoadProjects()
        {
            DataSet dts = new BCProject().GetAllProjects();
            try
            {
                if (dts.Tables.Count > 0)
                {
                    foreach (DataRow item in dts.Tables[0].Rows)
                    {
                        projects.Add(new Project() { Code = item["codigo"].ToString(), Name = item["nome"].ToString() });
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}